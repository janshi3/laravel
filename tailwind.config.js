module.exports = {
  purge: [
    './storage/framework/views/*.php',
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        purple: {
          'main': '#c4c4f6'
        }, 
  
        yellow: {
          'main': '#fe9a00'
        }, 
  
        gray: {
          'main': '#123b4f'
        }, 
      }, 

      fontSize: {
        'xxl': ['1.35rem', '1.85rem'],
      }, 

      letterSpacing: {
        'marketing': '.3em'
       }
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
