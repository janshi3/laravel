@extends('layouts.app')

@section('content')

<div class="flex flex-col text-center lg:text-left lg:flex-row h-full">
    <div class="lg:w-2/5">
        <div class="my-5 lg:my-10">
            <h1 class="text-6xl md:text-8xl font-bold mb-4 tracking-wide">SIGN UP</h1>
        </div>

        {{-- Form --}}

        <form action="/signup" method="post">
            @csrf
            <label class="my-4 text-2xl md:text-3xl text-yellow-main tracking-widest" for="name">Name: </label>
            <br>
            <input class="my-4 text-2xl md:text-3xl rounded-md border border-gray-main p-2" type="name" name="name" id="name">
            <br>
            <label class="my-4 text-2xl md:text-3xl text-yellow-main tracking-widest" for="email">Email: </label>
            <br>
            <input class="my-4 text-2xl md:text-3xl rounded-md border border-gray-main p-2" type="email" name="email" id="email">
            <br>
            <button class="my-4 text-xl tracking-wide font-bold text-white bg-purple-main rounded-full pt-3 pb-4 px-5 lg:ml-60" type="submit">Submit</button>
        </form>
    </div>
    <div class="w-full mt-16 lg:m-0 lg:h-full lg:w-3/5">
        <img class="w-full" src="{{ asset('storage/illustration.jpg') }}" alt="Illustration">
    </div>
</div>

@endsection