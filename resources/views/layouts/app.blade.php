<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <script>
        function toggleDropdown(){
            dropdown = document.getElementById("dropdown");
            dropdown.classList.toggle('hidden');
            dropdown.classList.toggle('flex');
        };
    </script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

{{-- Body --}}
<body class="h-screen w-screen tracking-tighter font-sans text-gray-main p-5 md:py-10 md:pl-12 md:pr-16">
    <div class="flex flex-col flex-grow">
        <div class="flex flex-col h-full" id="app">
    
            {{-- Navigation --}}
            <nav id="nav" class="flex flex-wrap md:flex-nowrap pb:16 lg:pb-24">
                {{-- Logo --}}
                <a class="font-bold text-3xl tracking-normal" href="/">
                    LOGO
                </a>

                <button class="ml-auto text-2xl md:hidden focus:outline-none" type="button" onclick="toggleDropdown()">
                    <i class="fa fa-bars"></i>
                </button>
                
                <!-- Right Side Of Navbar -->
                <ul id="dropdown" class="transition w-fill hidden flex-col text-center w-full justify-around md:flex md:flex-row lg:w-auto lg:min-w-1/2 lg:ml-auto">
                    <hr class="border-gray-main my-5 md:hidden">
                    <li class="my-2 md:my-0"><a href="/" class="text-xxl mx-auto lg:mx-6">Home</a></li>
                    <li class="my-2 md:my-0"><a href="/services" class="text-xxl mx-auto lg:mx-6">Services</a></li>
                    <li class="my-2 md:my-0"><a href="/about" class="text-xxl mx-auto lg:mx-6">About</a></li>
                    <li class="my-2 md:my-0"><a href="/contact" class="text-xxl mx-auto lg:mx-6">Contact</a></li>
                    <li class="my-2 md:my-0"><a href="/faq" class="text-xxl mx-auto lg:mx-6">FAQ</a></li>
                    <li class="my-4 md:my-0"><a href="/signup" class="text-xxl bg-purple-main rounded-full pt-3 pb-4 px-8 mx-auto lg:mx-6">SIGN UP</a></li>
                    <hr class="border-gray-main mt-6 md:hidden">
                </ul>
            </nav>
    
            {{-- Messages --}}
            {{-- Status --}}
            <main class="text-center">
                @if (session('status'))
                    <div class="bg-green-200 text-2xl mt-4 lg:mb-8 lg:-mt-16 py-2 rounded-lg border border-green-700 text-green-600">
                        {{ session('status') }}
                    </div>
                @endif
                
                {{-- Error --}}
                @if ($errors->any())
                    <div class="text-center mt-4 lg:mb-8 lg:-mt-16">
                        <ul class="bg-red-200 rounded-lg border border-red-700">
                            @foreach ($errors->all() as $error)
                                <li class="text-2xl py-2 text-red-600">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('error'))
                    <div class="bg-red-200 text-2xl py-2 rounded-lg border border-red-700 text-red-600">
                        {{ session('error') }}
                    </div>
                @endif
                @yield('content')
            </main>
        </div>
    </div>
</body>
</html>
