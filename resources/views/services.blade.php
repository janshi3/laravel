@extends('layouts.app')

@section('content')

<div class="flex flex-col text-center lg:text-left lg:flex-row h-full">
    <div class="lg:w-2/5">
        <div class="my-5 lg:my-10">
            <h1 class="text-6xl md:text-8xl font-bold mb-4 tracking-wide">SERVICES</h1>
        </div>

        <p class="tracking-wide text-lg text-gray-400 mb-10 lg:mb-24">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sequi recusandae, ad vitae hic consequatur 
            error ipsum facere quo minima quas tenetur, ipsa quidem suscipit et rerum atque? Temporibus, repellendus doloribus.
        </p>
        <a href="/about" class="text-xl tracking-wide font-bold text-white bg-purple-main rounded-full pt-3 pb-4 px-5">LEARN MORE</a>
    </div>
    <div class="w-full mt-16 lg:m-0 lg:h-full lg:w-3/5">
        <img class="w-full" src="{{ asset('storage/illustration.jpg') }}" alt="Illustration">
    </div>
</div>

@endsection