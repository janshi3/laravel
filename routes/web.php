<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesConttroller;
use App\Http\Controllers\UsersConttroller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\PagesController::class, 'index']);

Route::get('/services', [App\Http\Controllers\PagesController::class, 'services']);

Route::get('/about', [App\Http\Controllers\PagesController::class, 'about']);

Route::get('/contact', [App\Http\Controllers\PagesController::class, 'contact']);

Route::get('/faq', [App\Http\Controllers\PagesController::class, 'faq']);

Route::get('/signup', [App\Http\Controllers\UsersController::class, 'create']);

Route::post('/signup', [App\Http\Controllers\UsersController::class, 'store']);

