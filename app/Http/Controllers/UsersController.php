<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UsersController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('signup');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Check If All Fields Are Filled
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email'
        ]);

        // Create User Entry
        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
        ]);

        return redirect('/')->with('status', 'Signed Up Successfully!');
    }
}
