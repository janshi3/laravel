<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    //  Home Page
    public function index()
    {
        return view('index');
    }

    //  Services
    public function services()
    {
        return view('services');
    }

    //  About
    public function about()
    {
        return view('about');
    }

    //  Contact
    public function contact()
    {
        return view('contact');
    }

    //  FAQ
    public function faq()
    {
        return view('faq');
    }
}
